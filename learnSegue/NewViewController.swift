//
//  NewViewController.swift
//  learnSegue
//
//  Created by umar on 9/26/17.
//  Copyright © 2017 iOS. All rights reserved.
//

import UIKit

class NewViewController: UIViewController {
    var infoObject:String?     // variabel penyimpan nilai persiapan
    
    @IBOutlet weak var labelNewVc: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if infoObject != nil {         
            labelNewVc.text = infoObject
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
